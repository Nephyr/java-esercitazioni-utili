import java.util.*;

public class ProvaUnsafeList
{
    public static void main (String[] args)
    {
        //Vector<?> vInt = new Vector();
        //ArrayList<?> vInt = new ArrayList();
        
        List vInt = new ArrayList();
        
        vInt.add(0x002);
        vInt.add(2);
        vInt.add("ciao");
        vInt.add('c');
        vInt.add(5);
        vInt.add(6);
        
        for (int i=0; i<vInt.size(); i++)
            //System.out.println (vInt.elementAt(i));
            System.out.println (vInt.get(i));
    }
}