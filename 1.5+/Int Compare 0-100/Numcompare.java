/*
 * Intsort.java
 * Created on 21-10-2010 11:18 AM
 *
 */

import java.util.*;

public class Numcompare {
   
   /*
    *   PROCEDURA - MAIN
    * -------------------------------------
    *
    *   Return:     Void
    *   Input:      String[]
    *   Err.Chk:    True
    *
    */
   public static void main(String[] Args) {
       
       try{
            int x = 0;
            Scanner s = new Scanner(System.in);
            
            System.out.print("\n - Immettere un valore intero:  ");
            x = s.nextInt();
            
            if(x>=0 && x<=100)
            {
                if(x>=0 && x<=20) System.out.println(" - Very Low");
                else if(x>=20 && x<=40) System.out.println(" - Low");
                else if(x>=40 && x<=60) System.out.println(" - Normal");
                else if(x>=60 && x<=80) System.out.println(" - High");
                else System.out.println(" - Very High");
            }
            else System.out.println(" - Valore immesso non compreso tra 0 e 100!!");
       }
       catch(Exception e)
       {
           System.out.println(e);
       }
   }
   
}