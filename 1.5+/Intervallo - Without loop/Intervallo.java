/*
 * Intervallo.java
 * Created on 22-10-2010 05:32 PM
 *
 */

public class Intervallo {
   
   public static String PrintInterv(int _min, int _max)
   {
       if(_min < _max)
            return _min + " " + PrintInterv(_min + 1, _max);
       else
            return _max + " ";
   }
   
   public static void main(String[] Args) {
       int x = 1, y = 20;
       System.out.println("Intervallo:  " + PrintInterv(x, y));
   }
}
