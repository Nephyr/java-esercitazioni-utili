//===============================================================
//  PACKAGE
//===============================================================

import java.io.File;
import java.util.Vector;
import java.util.Scanner;

//===============================================================
//  START APP
//===============================================================

public class DirReader{

    /**
     *  Lettura di file e cartelle contenute 
     *  nella directory passata come parametro
     * -----------------------------------------
     *
     *  @param          _path:String
     *  @return         _i:int
     *  @var            _storage:Vector<File>
     */
    public byte GetTree(String _path)
    {
        // Variabili
        Vector<File> _storage = new Vector<File>();
        _storage.add(new File(_path));
        int _i = 0;
        
        //======= INIZIO LETTURA =======
        
        do {
            try {
                // Check: NullPointerException
                if(!_storage.get(_i).exists() || 
                   !_storage.get(_i).canRead())
                    return 0;
                
                // Ciclo lettura file(s) nella cartella
                for(File fs : _storage.get(_i).listFiles())
                {
                    // if( is directory ):
                    //     aggiungo al vettore _storage
                    if(fs.isDirectory() && !_storage.contains(fs))
                        _storage.add(fs);
                
                    // Stampo il percorso assoluto file/dir
                    System.out.print("\n\t--> " + fs.getAbsolutePath());
                }
            }
            // Gestione eccezioni (es: permessi)
            catch(Exception e) {
                System.out.print("\n\t--> SKIP: Impossibile accedere alla risorsa!");
            }
            
            // Check overflow interi
            if(_i+1 >= Integer.MAX_VALUE) {
                System.out.print("\n\t--> Altro...");
                return 1;
            }
        }
        // Ciclo lettura dir(s) in _storage
        while(_storage.size() > ++_i);
        
        //======= FINE LETTURA =======
        
        return 2;
    }
    
    /**
     *  Main Programma: DirReader
     * -----------------------------------------
     *  
     *  @param          args:String[]
     *  @var            _input:Scanner
     *  @var            _b:DirReader
     */
    public static void main(String[] args)
    {
        // Dichiarazioni
        DirReader _b = new DirReader();
        Scanner _input = new Scanner(System.in);
        
        // Input path assoluta
        System.out.print(" Percorso assoluto della directory: ");
        String _path = _input.nextLine();
        
        // Notifica errore
        switch( _b.GetTree(_path) )
        {
            case 0: // non esiste
                System.out.println("\n La directory specificata non esiste!");
                break;
                
            case 1: // overflow indice
                System.out.println("\n\n Lettura terminata dal programma.\n" + 
                                   " Albero directory troppo esteso!");
                break;
                
            case 2: // completata
                System.out.println("\n\n Lettura completata con successo!");
                break;
            
            default: // unknown
                System.out.println("\n ERRORE: Stato non gestito!");
                break;
        }
    }
    
}