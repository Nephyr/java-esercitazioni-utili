/*
*     Scrivere un programma Java che legge due numeri interi N e M
*     tramite l'apposito metodo della classe Scanner e stampa a video
*     N^M supponendo di non avere a disposizione l'operatore di
*     elevamento a potenza (ovvero utilizzando solo la moltiplicazione).
*
*/

import java.util.Scanner;

public class Esempio{

      public static void main(String[] args){

            Scanner _handle = new Scanner(System.in);
            int _n = 0, _m = 0, _res = 0;

            boolean DEBUG = true;

            System.out.print("Inserire il valore N:  ");
            _n = _handle.nextInt();

            System.out.print("Inserire il valore M:  ");
            _m = _handle.nextInt();

            for(int index = 0; index < _m; index++)
            {
                  if(index == 0)
                        _res = _n;
                  else
                        _res *= _n;

                  if(DEBUG){
                        System.out.println("DEBUG: " + _n +
                        "^" + ((_m - (_m - index)) + 1) +
                        " \t = " + _res);
                  }
            }

            System.out.println("\nValore del calcolo " + _n +
                        "^" + _m + ":  " + _res);
      }

}
