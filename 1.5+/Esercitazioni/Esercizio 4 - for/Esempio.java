/*
*     Scrivere un programma Java composto dai seguenti metodi:
*
*           1. metodo stampa: riceve come parametri un numero
*              intero N e un carattere c. Successivamente, se N è
*              pari stampa N volte c, e se N è dispari stampa c un
*              numero di volte uguale al maggiore numero pari
*              più piccolo di N.
*
*           2. metodo main: chiama il metodo stampa passando come
*              parametri un intero letto tramite l'apposito metodo
*              della classe Scanner e il carattere costante 'a'.
*
*/

import java.util.Scanner;

class MyUtils{

      /*
       *    COSTRUTTORE
       */
      MyUtils(){
            // blank
      }

      String _err = "\n\t Il valore( INPUT ) non e' rappresentabile!" +
                    "\n\t Possibili cause scatenanti:" +
                    "\n\n\t\t - Valore troppo piccolo." +
                    "\n\t\t - Overflow variabile intera." +
                    "\n\t\t - Input Type errato.";

      /*
       *    METODO - STAMPA
       * ----------------------------------------------
       *
       *    Input1:           Integer
       *    Input2:           Char
       *    Return:           Void
       */
      public void Stampa(int _end, char _ch){
            if(_end%2 != 0)
                  _end = PairFinder(_end);

            if(_end == 0)
                  System.out.println(_err);
            else{
                  System.out.print("\n\t- Numero pari usato\t= " + _end);
                  System.out.print("\n\t- Sequenza caratteri\t= ");

                  do{
                        System.out.print(_ch);
                  }while( --_end > 0 );
            }
            return;
      }

      /*
       *    METODO - PAIRFINDER
       * ----------------------------------------------
       *
       *    Input1:           Integer
       *    Return:           Integer
       */
      public int PairFinder(int _value){
            do{
                  if(_value%2 == 0)
                        return _value;
            }while( --_value > 0 );
            return 0;
      }
}

public class Esempio{


      public static void main(String[] args){

            MyUtils _MyUtils = new MyUtils();
            Scanner _handle = new Scanner(System.in);

            int _n = 0;

            //------------------------------------------

            try{
                  System.out.print("Inserire il valore intero N:  ");
                  _n = _handle.nextInt();
            }
            catch(Exception e){
                  _n = 0;
            }

            _MyUtils.Stampa(Math.abs(_n), 'a');
      }

}
