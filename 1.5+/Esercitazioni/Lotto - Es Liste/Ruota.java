public class Ruota
{
    private String      citta;
    private int         numeri[];
    private Ruota       next;

    public Ruota(){ next = null; }
    public void setCitta(String c){ citta = c; }
    public void setNumeri(int n[]){ numeri = n; }
    public void setNext(Ruota nextItr){ next = nextItr; }
    public Ruota getNext(){ return next; }
    public boolean hasNext(){ return (next == null) ? false : true; }

    public String toString(){
        String tmpLog   = citta + ": ";
        int itrInt      = 0;
        do{
            tmpLog += numeri[itrInt] + " ";
        }while (++itrInt < numeri.length);
        return tmpLog;
    }
}
