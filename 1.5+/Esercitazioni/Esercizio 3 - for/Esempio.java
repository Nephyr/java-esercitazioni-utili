/*
*     Scrivere un programma Java composto dai seguenti metodi:
*
*           1. metodo moltiplica: riceve come parametri due numeri
*              double N e M e restituisce il prodotto tra N e M
*              supponendo di non poter utilizzare l'operatore
*              di moltiplicazione standard di Java *.
*
*           2. metodo main: legge un numero intero N tramite l'apposito
*              metodo della classe Scanner e stampa a video il
*              quadrato di tutti i numeri compresi tra 1 e N
*              (estremi compresi). In altri termini, andrà stampato
*              un messaggio come il seguente:
*
*                 - il quadrato di 1 è 1
*                 - il quadrato di 2 è 4
*                   ...
*                 - il quadrato di N è ...
*
*     si utilizzi il metodo moltiplica per calcolare i prodotti,
*     ovunque ve ne sia necessità.
*
*/

import java.util.Scanner;

class MyMath{

      /*
       *    COSTRUTTORE
       */
      MyMath(){
            _tmp = 0.0;
      }

      /*
       *    VARIABILI - GLOBALI
       */
      private double _tmp;

      /*
       *    METODO - MOLTIPLICA
       * ----------------------------------------------
       *
       *    Input1:           Void
       *    Return:           Void
       */
      public void Reset(){
            _tmp = 0.0;
            return;
      }

      /*
       *    METODO - MOLTIPLICA
       * ----------------------------------------------
       *
       *    Input1:           Double
       *    Input2:           Double
       *    Temp.Var:         Double
       *    Return:           Double
       */
      public double Mol(double _first, double _second){
            do{
                  _tmp += _first;
            }while( --_second > 0 );
            return _tmp;
      }

      /*
       *    METODO - QUADRATO
       * ----------------------------------------------
       *
       *    Input1:           Double
       *    Return:           Void
       */
      public void Quad(double _end){
            System.out.println("");
            do{
                  Reset();
                  System.out.println("\t- Quadrato di " + _end +
                  " \t = " + Mol(_end, _end));
            }while( --_end > 0 );
            return;
      }

}

public class Esempio{

      public static void main(String[] args){

            MyMath _MyMath = new MyMath();
            Scanner _handle = new Scanner(System.in);
            double _n = 0.0, _m = 0.0;

            //------------------------------------------

            System.out.print("Inserire il valore N:  ");
            _n = _handle.nextDouble();

            System.out.print("Inserire il valore M:  ");
            _m = _handle.nextDouble();

            System.out.println("\n\t- Il prodotto N * M \t = " +
                  _MyMath.Mol(_n, _m) + "\n");

            //------------------------------------------

            System.out.print("Inserire estremo [1 a ?]:  ");
            _n = _handle.nextDouble();

            _MyMath.Reset();
            _MyMath.Quad(_n);
      }

}
