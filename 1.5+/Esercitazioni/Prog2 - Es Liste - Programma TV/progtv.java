/*
 *    TESTO ESERCIZIO
 * ---------------------------------
 *
 *    Scrivere un programma che definisce:
 *
 *    1) Una classe Orario, caratterizzata da un'ora (dalle 0 alle 23) e dai minuti (da 0 a 59).
 *    2) Una classe Spettacolo, caratterizzata da un Orario di inizio e un Orario di fine.
 *    3) Una classe Palinsesto, caratterizzata da una lista di Spettacoli.
 * 
 *    Tale lista deve essere un attributo pubblico del Palinsensto, pertanto l'aggiunta e 
 *    rimozione di Spettacoli pu� avvenire con accesso pubblico alla lista.
 *    La classe definisce anche un metodo durataTotale(), che restituisce il numero complessivo 
 *    di minuti di spettacoli del palinsesto.
 *  
 *    4) Una classe EmittenteTV con un metodo main che:
 *    
 *          - istanzia due Palinsensti (Rete1 e Rete2);
 *          - istanzia per il Palinsesto Rete1 una serie di 4 Spettacoli;
 *          - per ciascuno spettacolo, istanzia un Orario di inizio ed un Orario di fine;
 *          - imposta ora e minuti di entrambi gli orari, con valori a scelta (purch� l'orario 
 *            di fine sia successivo a quello di inizio);
 *          - imposta i due orari cos� creati come Orario di inizio e Orario di fine del 
 *            corrispettivo Spettacolo
 *          - aggiunge ciascuno dei 4 Spettacoli cos� creati alla lista degli Spettacoli del 
 *            Palinsensto Rete1
 *          - ripete il tutto per il Palinsensto Rete2 (limitandosi a 2 soli Spettacoli)
 *          - visualizza, per ciascun Palinsesto, la durata totale (in minuti) degli Spettacoli.
 *
 * ---------------------------------
 *    Produttore: Vanzo Luca Samuele
 * ---------------------------------
 *    Import utili al programma.
 *
 */
 
import java.util.ArrayList;
import java.util.Scanner;

/*
 *    CLASS - ORARIO
 * ---------------------------------
 *
 */
class orario{

      private short ora, minuti;
      
      public orario(){
            ora = 0;
            minuti = 0;
      }
      
      private boolean _check_value(short v, boolean c){
            if( c )
                  return (v > 0 && v < 60) ? true : false;
            else
                  return (v > 0 && v < 24) ? true : false;
      }
      
      public short GetOre(){
            return ora;
      }
      
      public short GetMinuti(){
            return minuti;
      }
      
      public void SetOre(short o){
            if(_check_value(o, false))
                  ora = o;
            else{
                  System.out.println("\n - Valore orario [ORE] errato.\n" + 
                                     "   Immettere un valore compreso tra 0 e 23!");
                  System.exit(0);
            }
      }
      
      public void SetMinuti(short m){
            if(_check_value(m, true))
                  minuti = m;
            else{
                  System.out.println("\n - Valore orario [MINUTI] errato.\n" + 
                                     "   Immettere un valore compreso tra 0 e 59!");
                  System.exit(0);
            }
      }
      
}

/*
 *    CLASS - SPETTACOLO
 * ---------------------------------
 *
 */
class spettacolo{

      private orario ora_start, ora_finish;
      
      public spettacolo(){
            ora_start = new orario();
            ora_finish = new orario();
      }
      
      public orario GetStarter(){
            return ora_start;
      }
      
      public orario GetFinisher(){
            return ora_finish;
      }

}

/*
 *    CLASS - PALINSESTO
 * ---------------------------------
 *
 */
class palinsesto{

      public ArrayList<spettacolo> db;
      
      public palinsesto(){
            db = new ArrayList<spettacolo>();
      }

      public String durataTotale(){
            int durata = 0;
            for(spettacolo _tmp : db){
                  durata += (((_tmp.GetFinisher().GetOre() * 60) + 
                            _tmp.GetFinisher().GetMinuti()) - 
                            ((_tmp.GetStarter().GetOre() * 60) + 
                            _tmp.GetStarter().GetMinuti()));
            }
            return Integer.toString(durata) + " Minuti";
      }
      
}

/*
 *    MAIN CLASS
 * ---------------------------------
 *    Entry point.
 *
 */
public class progtv{

      private static void _debug(palinsesto _data){
            for(spettacolo _tdata : _data.db)
            {
                  System.out.println(" - Orario Spettacolo: Inizio( " + 
                                     _tdata.GetStarter().GetOre() + ":" +
                                     _tdata.GetStarter().GetMinuti() +
                                     " ), Fine( " + 
                                     _tdata.GetFinisher().GetOre() + ":" +
                                     _tdata.GetFinisher().GetMinuti() + " )");
            }
      }

      public static void main(String[] args){
      
            palinsesto Rete1, Rete2;
            Rete1 = new palinsesto();
            Rete2 = new palinsesto();
            
            Rete1.db.add(new spettacolo());
            Rete1.db.add(new spettacolo());
            Rete1.db.add(new spettacolo());
            Rete1.db.add(new spettacolo());
            
            short ora = 13;
            
            for(spettacolo _r1 : Rete1.db)
            {
                  _r1.GetStarter().SetOre( ora );
                  _r1.GetStarter().SetMinuti( (short)5 );
                  
                  _r1.GetFinisher().SetOre( ora++ );
                  _r1.GetFinisher().SetMinuti( (short)55 );
            }
            
            Rete2.db.add(new spettacolo());
            Rete2.db.add(new spettacolo());
            
            ora = 15;
            
            for(spettacolo _r2 : Rete2.db)
            {
                  _r2.GetStarter().SetOre( ora );
                  _r2.GetStarter().SetMinuti( (short)15 );
                  
                  _r2.GetFinisher().SetOre( ora++ );
                  _r2.GetFinisher().SetMinuti( (short)45 );
            }
            
            System.out.println("\n - Durata totale Palinsesto Rete1: " + Rete1.durataTotale());
            System.out.println(" - Durata totale Palinsesto Rete2: " + Rete2.durataTotale());
            
            System.out.println("\n Debug Rete1:\n");
            _debug(Rete1);
            
            System.out.println("\n Debug Rete2:\n");
            _debug(Rete2);
      }

}