/*
*     Scrivere un programma che legge una sequenza di numeri interi tramite l'apposito
*     metodo della classe Scanner. Il programma deve contare quante volte, in questa sequenza,
*     e' contenuto il numero 10 e stampare questa informazione a video.
*     Si supponga che la sequenza termini quando viene letto il numero 0.
*
*/

import java.util.Scanner;

public class Esempio{

      public static void main(String[] args){

            Scanner _handle = new Scanner(System.in);
            double _min = Double.MAX_VALUE, _tmp = 0.0;

            for(int n = -1; ((n >= 0) ? n > 0 : n < 0); n--)
            {
                  if(n < 0 && _tmp == 0.0){
                        System.out.print("Inserire un valore positivo:  ");
                        //---->
                        n = _handle.nextInt();
                        //---->
                        if(n > 0) n++;
                        else if(n == 0){
                              System.out.println("\n - Nessun valore inserito!");
                              return;
                        }
                  }
                  else
                  {
                        System.out.print("Inserire il valore double #" + n + ":  ");
                        //---->
                        _tmp = _handle.nextDouble();
                        //---->
                        if(_tmp < _min)
                              _min = _tmp;
                  }
            }

            System.out.println("\n - Valore minimo:  " + _min);
      }

}
