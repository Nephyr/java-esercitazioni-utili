import java.util.*;

public class EliminaVocali{

    public String stringCleaner(String _str, int _index)
    {
        if( _str.length() <= _index ) 
            return "";
        
        // charAt => carattere in posizione N
        if( "aeiou".indexOf( _str.charAt(_index) ) > -1 )
            return /*NULL*/ stringCleaner(_str, ++_index);
        else 
            return _str.charAt(_index) + stringCleaner(_str, ++_index);
    }

    public static void main(String[] args){
        EliminaVocali b = new EliminaVocali();
        System.out.println("Stringa senza vocali: " + b.stringCleaner(args[0], 0));
    }

}