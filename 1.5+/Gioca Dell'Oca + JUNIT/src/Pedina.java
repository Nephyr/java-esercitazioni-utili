public class Pedina {
    
    private String colore          = null;
    private int    casellaCorrente = 0;
    
    public Pedina(String setColore) {
        colore = setColore;
        casellaCorrente = 1;
    }
    
    public void avanza ( int lancio ) {
        casellaCorrente += lancio;
    }
    
    public int getCasellaCorrente () {
        return casellaCorrente;
    }
    
    @Override public String toString () {
        return "Pedina: \n * Colore: " + colore +
               " \n * Casella Corrente: " + casellaCorrente +
               " \n\n Eliminata o ha finito il turno.";
    }
    
}
