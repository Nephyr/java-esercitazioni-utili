import java.util.Vector;

public class GiocoDellOca {
    
    private int            numeroCaselleTotali = 0;
    private int            turnoCorrente       = 0;
    
    private Dado           _dado               = null;
    private Vector<Pedina> _pedine             = null;
    
    public GiocoDellOca(int caselleTotali) {
        _dado = new Dado();
        _pedine = new Vector<Pedina>();
        numeroCaselleTotali = caselleTotali;
    }
    
    public int getTurnoCorrente () {
        return turnoCorrente;
    }
    
    public int setTurnoCorrenteAndReturn () {
        return ++turnoCorrente;
    }
    
    public void setTurnoCorrente ( boolean reset ) {
        if (reset) turnoCorrente = 0;
        else ++turnoCorrente;
    }
    
    public int getNumeroCaselleTotali () {
        return numeroCaselleTotali;
    }
    
    public Dado getDado () {
        return _dado;
    }
    
    public Vector<Pedina> getPedine () {
        return _pedine;
    }
    
    public Pedina giocaTurno () {
        if (turnoCorrente >= _pedine.size()) turnoCorrente = 0;
        
        _pedine.get(turnoCorrente).avanza(_dado.lancia());
        if (_pedine.get(turnoCorrente).getCasellaCorrente() >= numeroCaselleTotali) {
            // Tolgo e ritorno.
            return _pedine.remove(turnoCorrente++);
        }
        
        ++turnoCorrente;
        return null;
    }
    public void aggiungiPedina ( Pedina p ) {
        if (p != null) _pedine.add(p);
        else System.out.println("Error: Into 'aggiungiPedina()' variable p is NULL");
    }
}
