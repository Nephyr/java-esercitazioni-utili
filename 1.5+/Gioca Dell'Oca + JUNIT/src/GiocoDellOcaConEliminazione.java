public class GiocoDellOcaConEliminazione extends GiocoDellOca {
    
    private int casellaEliminazione = 0;
    
    public GiocoDellOcaConEliminazione(int caselleTotali, int casellaElim) {
        super(caselleTotali);
        casellaEliminazione = casellaElim;
    }
    
    @Override public Pedina giocaTurno () {
        if (getTurnoCorrente() >= getPedine().size()) setTurnoCorrente(true);
        
        getPedine().get(getTurnoCorrente()).avanza(getDado().lancia());
        if (getPedine().get(getTurnoCorrente()).getCasellaCorrente() >= getNumeroCaselleTotali() ||
            getPedine().get(getTurnoCorrente()).getCasellaCorrente() == casellaEliminazione) {
            // Tolgo e ritorno.
            return getPedine().remove(setTurnoCorrenteAndReturn());
        }
        
        setTurnoCorrente(false);
        return null;
    }
}
