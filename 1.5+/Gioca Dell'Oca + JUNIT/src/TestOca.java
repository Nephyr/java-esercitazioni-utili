import junit.framework.TestCase;

public class TestOca extends TestCase {
    
    public void test () {
        GiocoDellOca gioco = new GiocoDellOca(6);
        Pedina prima = new Pedina("red");
        
        gioco.aggiungiPedina(prima);
        gioco.aggiungiPedina(new Pedina("blue"));
        gioco.aggiungiPedina(new Pedina("green"));
        
        Pedina testPrima = null;
        while (true) {
            if (testPrima == prima) break;
            else testPrima = gioco.giocaTurno();
        }
        
        System.out.println(testPrima.toString());
        
        assertSame(prima, testPrima);
        assertTrue(testPrima.getCasellaCorrente() >= 6);
    }
}
