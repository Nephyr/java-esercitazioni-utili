public class Prova{
    
    public static void main(String[] args){

        Triangolo t = new Triangolo(3, 4, 10, 12);
        System.out.println("Triangolo:");
        System.out.println("  - Area:  " + t.calcArea());
        System.out.println("  - Perimetro:  " + t.calcPerim());
        
        Rettangolo r = new Rettangolo(3, 4);
        System.out.println("\nRettangolo:");
        System.out.println("  - Area:  " + r.calcArea());
        System.out.println("  - Perimetro:  " + r.calcPerim());
        
    }

}