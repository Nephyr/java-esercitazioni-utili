public class Triangolo extends Poligono{

    private int base = 0;
    private int altezza = 0;
    private int lato1 = 0;
    private int lato2 = 0;
    
    public Triangolo(int b, int h, int l1, int l2){
        super( 3 );
        
        base = b;
        altezza = h;
        lato1 = l1;
        lato2 = l2;
    }
    
    @Override
    public int calcArea(){
        return (base * altezza)/2;
    }
    
    @Override
    public int calcPerim(){
        return (base + lato1 + lato2);
    }
    
}