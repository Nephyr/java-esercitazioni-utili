<html>
	<head>
    	<title>Applet Socket</title>
        <style>
			
			body{
					background-color: #eee;
					font-family: Verdana, Geneva, sans-serif;
					font-size: 120%;
					color:#333;
					text-align: center;
				}
				
			table.applet{
					text-align: center;
				}
				
			div.windowed{
					background-color: #333;
					max-width: 500px;
					padding: 3px;
					text-align: center;
					border: 1px solid #fff;
				}
				
			table.applet applet{
					border: 1px solid #fff;
					background-color:#FFF;
				}
				
			table.applet div.windowed div{
					background-color:#FFF;
					max-width: 500px;
					border: 1px solid #fff;
					padding: 5px;
					font-size: 80%;
					color: #333;
					margin-bottom: 1px;
				}
			
		</style>
    </head>
    <body>
    	<table cellpadding="0" cellspacing="0" border="0" width="100%" class="applet">
        	<tbody>
            	<tr>
                	<td width="100%" align="center" style="padding: 60px 0px 0px 0px;">
                        <div class="windowed">
                            <div><b>APPLET - CONNESSIONE SOCKET</b></div>
                            <applet codebase="." code="SocketApplet.class" width="500" height="150">
                                <param name="RemoteEndPoint" value="79.37.187.173">
                                <param name="RemotePort" value="3726">
                            </applet>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>