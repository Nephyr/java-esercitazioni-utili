import java.applet.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;



public class SocketApplet extends Applet {
    private Label aLabel;
    private String host;
    private int port;
    private SocketPermission sp;
    
    public void init() {
        try 
        {  
            Socket sock = null;
            String response = "";
            String sendstr = "";
            byte[] buffer = new byte[12];
    
            setLayout(new GridBagLayout());
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.weightx = 500.0;
            gbc.weighty = 150.0;
            
            gbc.anchor = GridBagConstraints.CENTER;
            aLabel = new Label("", Label.CENTER);
            add(aLabel, gbc);
            
            host = getParameter("RemoteEndPoint");
            port = Integer.parseInt(getParameter("RemotePort"));
            
            if(System.getSecurityManager()==null)
            {
                System.setProperty("java.security.policy","http://www.wowsoc.org/tmp/applet/no.policy");
                System.setSecurityManager(new RMISecurityManager());
            }
            
            sock = new Socket(host, port);
            showStatus("Applet. Running...");
            
            //--------------------------------------------------------- NEGOZIAZIONE
            
            OutputStream out = sock.getOutputStream();
            InputStream in = sock.getInputStream();
            
            sendstr = "0x003[FINAL]";
            out.write(sendstr.getBytes(), 0, 12);
            
            in.read(buffer);
            response = new String(buffer);
            
            if(!response.equals("0x001[FINAL]"))
            {
                aLabel.setText("Servizio temporaneamente non disponibile.\nRiprovare tra qualche minuto.");
                if (sock != null)
                    sock.close();
                    
                    return;
            }
            
            sendstr = "0x00F[FINAL]";
            out.write(sendstr.getBytes(), 0, 12);
            
            in.read(buffer);
            response = new String(buffer);
            
            if(response.equals("0x010[FINAL]"))
            {
                aLabel.setText("Registrazione effettuata con successo.");
                showStatus("Applet. Complete!!");
            }
            else
                aLabel.setText("Impossibile effettuare la registrazione.");
            
            sendstr = "0x004[FINAL]";
            out.write(sendstr.getBytes(), 0, 12);
            
            if (sock != null)
                sock.close();
        } 
        catch (Exception e) {
           aLabel.setText(e.getMessage());
        }
    }
}